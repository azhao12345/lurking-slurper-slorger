'use strict';

// Test bot
var fs = require('fs');
var bot = require('telegram-bot-bootstrap');

var Alice = new bot('api key here');

// get your chat_id from here
//Alice.getUpdates({timeout: 6}).then(console.log);
//Alice.getUpdates(704030684).then(console.log);

var sources = {};
var sinks = {};



var handleUpdates = function(response)
{
    console.log(response);
    response = JSON.parse(response);
    if(!response.ok)
    {
        console.log('response not ok start panicking');
    }
    else
    {
        console.log('processing response');

        var updates = response.result;
        var updateId = 0;
        for(var i = 0; i < updates.length; i++)
        {
            console.log('looking at update: ' + i);
            var update = updates[i];
            updateId = update.update_id;
            
            var chatId = update.message.chat.id;

            if(!update.message.text)
            {
                update.message.text = '';
            }

            var text = update.message.text;

            var split = text.split(' ');
            
            //check what the message is
            if(split[0] == '\/slorg')
            {
                var channel = split[1];

                console.log('slorgin');
                if(!sinks[channel])
                {
                    sinks[channel] = {};
                }

                sinks[channel][chatId] = true;
            }
            else if(split[0] == '\/slurp')
            {
                var channel = split[1];

                console.log('slurpin');
                if(!sources[chatId])
                {
                    sources[chatId] = {};
                }

                sources[chatId][channel] = true;
            }
            else
            {
                console.log(sources);
                console.log(sinks);
                console.log(chatId);
                //console.log(sources.indexOf(chatId))
                //check if the message is from a source
                if(sources[chatId])
                {
                    var dests = {};
                    //loop over all channels to broadcast
                    for(var channel in sources[chatId])
                    {
                        for(var sink in sinks[channel])
                        {
                            dests[sink] = true;
                        }
                    }

                    for(var dest in dests)
                    {
                        console.log('trying to forward');
                        Alice.forwardMessage(dest, chatId, update.message.message_id);
                        console.log('forwarded');
                    }
                }
            }
        }

        console.log('go for next');

        Alice.getUpdates({offset: updateId + 1, timeout: 60000}).then(handleUpdates);
    }
};


Alice.getUpdates().then(handleUpdates);

// try sending a message, and log the HTTP call for confirmation
// Alice.sendMessage('your-chat-id', 'Hey wanna see some cool art?').then(console.log);

// Alice.sendPhoto('your-chat-id', fs.createReadStream(__dirname+'/alexiuss.jpg'), 'Chronoscape by Alexiuss').then(console.log)


// var kb = {
//         keyboard: [
//             ['one'],
//             ['two', 'three'],
//             ['four', 'five', 'six']
//         ],
//         one_time_keyboard: true
//     };
// Alice.sendMessage('your-chat-id', "Choose a lucky number", undefined, undefined, kb)





